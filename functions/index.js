const functions = require('firebase-functions'); //run server code on the cloud
const admin = require('firebase-admin'); //fetch firestore data with admin rights
const express = require('express'); //write the routes of the application
const cors = require('cors'); //handle cors issues
const morgan = require('morgan'); //log request info on the console
const bodyParser = require('body-parser'); //get POST request data
const unirest = require('unirest'); // make HTTP calls with ease

//create our application with express.js
const app = express();
//initialize firebase application configurations (database path, storage path etc.)
admin.initializeApp(functions.config().firebase);
//set up firestore reference for reading/writing data
const firestore = admin.firestore();

app.use(morgan('dev')); //log every request to the console
app.use(bodyParser.json()); //parse application/json
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/**
 * Executes when a new transaction gets created and updates customers document of
 * the store.
 */
exports.createTransaction = functions.firestore
  .document('transactions/{transactionId}').onCreate(event => {
    const transaction = event.data.data(); // hold a reference to the transaction object

    return firestore.doc(`stores/${transaction.storeId}/customers/${transaction.customerId}`).get().then(customer => {
      if (customer.exists) { // If the customer is already saved in the stored just update the necessary fields
        let customerData = customer.data();
        return customer.ref.set({
          "numberOfTransactions": customerData.numberOfTransactions + 1,
          "amountOfTransactions": customerData.amountOfTransactions + transaction.transactionAmount,
          "amountOfCashback": customerData.amountOfCashback + transaction.cashbackAmount,
          "lastTransaction": transaction.transactionDate
        }, {merge: true}).then(() => {
          console.log("Transaction completed");
        }).catch(error=> {
          throw new Error("Transaction could not be completed");
        });
      } else { // If customer does not exist create him...
        return customer.ref.set({
          "customerId": transaction.customerId,
          "customerName": transaction.customerName,
          "numberOfTransactions": 1,
          "amountOfTransactions": transaction.transactionAmount,
          "amountOfCashback": transaction.cashbackAmount,
          "firstTransaction": transaction.transactionDate,
          "lastTransaction": transaction.transactionDate,
          "numberOfStamps": 0,
          "numberOfPresents": 0
        }).then(() => { // ...and update store notifications number
          firestore.doc(`stores/${transaction.storeId}`).get().then(store => {
            let storeData = store.data();
            console.log(storeData);
            return store.ref.set({
              "numberOfUnreadNotifications": storeData.numberOfUnreadNotifications + 1
            }, {merge: true}).then(()=> {
              console.log("Transaction completed");
            }).catch(error => {
              throw new Error(error);
            });
          }).catch(error=> {
            throw new Error(error);
          });
        }).catch(error=> {
          throw new Error(error);
        });
      }
    }).catch(error => {
      throw new Error(error);
    });
});