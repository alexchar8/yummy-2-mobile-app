import { Component, Input } from '@angular/core';
import { Store } from "../../models/store-model";
import { Events } from "ionic-angular";
import { MerchantDetailsPage } from "../../pages/store-details/store-details";
import { NavController } from "ionic-angular";
import { ServiceLocator } from '../../handlers/service-locator';
import { SqlStorageService } from '../../providers/sql-storage/sql-storage-service';
/**
 * Component used for visualizing the {@link Store} model in the form of card.
 *
 * @summary Component used for visualizing the {@link Store} model in the form of card.
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 14:11:00 
 * Last modified  : 2018-03-09 15:45:33
 */
@Component({
  selector: 'store-card',
  templateUrl: 'store-card.html'
})

export class StoreCard {
  /**
   * Store data
   */
  @Input() store: Store;

  /**
   * Placeholder image url to be displayed while real images are still loading
   */
  defaultImage: string = "assets/images/placeholder.png";
  /**
   * Placeholder store logo url to be displayed while real logos are still loading
   */
  defaultLogo: string = "assets/images/logo_placeholder.png";
  /**
   * Lazy loader image rendering offset
   */
  offset: number = 2000;

  /**
   * Empty constructor
   * @param events Events emitter
   * @param navCtrl Navigation controller
   */
  constructor(private events: Events, private navCtrl: NavController) {
  }

  /** Change merchant's following status when the "Add to favourite" button of the card is pressed */
  changeFollowingStatus() {
    this.store.changeFollowingStatus(this.events);
  }
  /**
   * Navigate to merchant ( this function isn't called for now and may be removed)
   */
  openMerchantPage() {
    this.navCtrl.push(MerchantDetailsPage, { store: this.store });
  }
}
