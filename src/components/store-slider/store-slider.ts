import { Component, Input } from '@angular/core';
import { StoreSlider } from "../../models/store-slider-model";
import { NavController } from "ionic-angular";
/**
 * Component used for visualizing the {@link StoreSlider} application model
 *
 * @summary Component used for visualizing the {@link StoreSlider} application model
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 17:38:13 
 * Last modified  : 2018-03-09 11:58:23
 */
@Component({
  selector: 'store-slider',
  templateUrl: 'store-slider.html'
})

export class StoreSliderComponent {
  /**
   * Store slider data object
   */
  @Input() storeSlider: StoreSlider;
  /**
   * Whether this slider is the last in home page list or not
   */
  @Input() isLastItem: boolean;

  /**
   * Placeholder image url to be displayed while real images are still loading
   */
  defaultImage: string = "assets/images/placeholder.png";
  /**
   * Lazy loader image rendering offset
   */
  offset: number = 2000;

  /**
   * Empty constructor
   * @param navCtrl Navigation controller
   */
  constructor(private navCtrl: NavController) {
    
  }
}
