import { Component } from '@angular/core';
/**
 * Visualises a card component with two sides that flips when clicked by the user.
 *
 * @summary Visualises a card component with two sides that flips when clicked by the user.
 * @author John Fanidis
 *
 * Created at     : 2018-02-13 17:00:35 
 * Last modified  : 2018-03-09 15:18:59
 */
@Component({
  selector: 'flash-card',
  templateUrl: 'flash-card.html'
})
export class FlashCardComponent {
  /**
   * Shows whether the card is flipped or not.
   */
  flipped: boolean;

  /**
   * Sets flipped variable to false.
   */
  constructor() {
    this.flipped = false;
  }

  /**
   * Inverts the face of the card when user clicks on the card.
   */
  flip(){
    this.flipped = !this.flipped;
  }

}
