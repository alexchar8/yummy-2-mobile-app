import { Component, Input } from '@angular/core';
/**
 * Show download progress with a loading bar
 *
 * @summary Show download progress with a loading bar
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 17:51:44 
 * Last modified  : 2018-03-09 15:20:05
 */
@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html'
})
export class ProgressBarComponent {
  /**
   * The current value of the progress bar
   */
  @Input('progress') progress;

  /**
   * Empty constructor
   */
  constructor() {}

}
