import { Component, Input } from '@angular/core';
import { YummyService } from "../../models/yummy-service-model";
/**
 * Component used for visualizing a {@link YummyService}
 *
 * @summary Component used for visualizing a Yummy service
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 12:16:46 
 * Last modified  : 2018-03-09 15:21:28
 */
@Component({
  selector: 'yummy-service',
  templateUrl: 'yummy-service-button.html'
})

export class YummyServiceComponent {
  /**
   * Yummy service data
   */
  @Input() service: YummyService;
  /** 
   * Background and text color of the store that this service is attached to.
   */
  @Input() colors: { background: string, text: string };

  /** 
   * Empty constructor.
   */
  constructor() { }
}
