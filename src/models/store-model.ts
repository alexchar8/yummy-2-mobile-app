import { Events } from "ionic-angular";
import { YummyService } from "./yummy-service-model";
import { SqlStorageService } from "../providers/sql-storage/sql-storage-service";
import { ServiceLocator } from '../handlers/service-locator';
/**
 * Represents a yummy store as saved in the local SQL database.
 *
 * @summary Application store model
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 10:49:59
 * Last modified  : 2018-03-08 18:15:43
 */
export class Store {
  /**
   * Whether or not this store is followed by the user.
   */
  followed: boolean;
  /**
   * Whether or not this store has enabled ibank payments.
   */
  hasIbankPay: boolean;

  /**
   * Empty constructors
   * @param id Store yummy id
   * @param name Store legal name
   * @param logo Store logo url
   * @param image Store card background image url
   * @param services Array of yummy services available for this store
   * @param colors Color configuration for this store's service buttons (found in {@link HomePage})
   */
  constructor(
    private id: number,
    private name: string,
    private logo: string,
    private image: string,
    private services: Array<YummyService>,
    private colors: { background: string, color: string }
  ) {
    let storage = ServiceLocator.injector.get(SqlStorageService);
    this.followed = storage.isStoreInFavourites(id);
    this.hasIbankPay = false;
  }

  /**
   * Follows/Unfollows merchant. When following status is changed, the corresponding event 
   * is published and we subscribe to both events in {@link HomePage}.
   * @param {Events} events Event emitter for broadcasting follow/unfollow events
   */
  changeFollowingStatus(events: Events) {
    this.followed = !this.followed;
    if (this.followed) {
      events.publish('store:started-following', this);
    } else {
      events.publish('store:stopped-following', this);
    }
  }

  /** Returns true if the store is followed by the user. Otherwise returns false. */
  isFollowed(): boolean {
    return this.followed;
  }

  /**
   * Switches the ibank pay service availability for this store
   */
  hasIBankPay(serviceStatus: boolean) {
    this.hasIbankPay = serviceStatus;
  }

  /**
   * Returns store id
   */
  getId(): number {
    return this.id;
  }
}
