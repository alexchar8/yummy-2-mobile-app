/**
 * Represents a service(way of earning money) a yummy store provides
 *
 * @summary Application yummy service model.
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 11:32:16 
 * Last modified  : 2018-03-09 15:22:39
 */
export class YummyService {
  /**
   * Textual description of the service.
   */
  name: string;
  /** 
   * Visual description of the service (cashback percent symbol or icon).
   */
  content: string;

  /**
   * Initializes service name and content based on the given service type.
   * @param type Yummy service type (1 = Cashback, 2 = Stamps, 3 = IbankPay)
   * @param cashback Cashback percentage (Only used when type = 1)
   */
  constructor(private type: number, private cashback?: number) {
    switch (type) {
      case 1:
        this.name = 'Επιστροφή μετρητών';
        this.content = (cashback * 100).toFixed(1) + '%';
        break;
      case 2:
        this.name = 'Δωροσφραγίδες';
        this.content = 'icomoon-stamp';
        break;
    }
  }
}
