import { Store } from "./store-model";
import { YummyService } from "./yummy-service-model";
import { SqlStorageService } from "../providers/sql-storage/sql-storage-service";
/**
 * Represents a slider of yummy stores as saved in the local SQL database
 *
 * @summary Represents a slider of yummy stores as saved in the local SQL database
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 12:38:18 
 * Last modified  : 2018-03-09 15:22:31
 */
export class StoreSlider {
  /**
   * Array of the top stores displayed on the front row of slider (in {@link HomePage})
   */
  promotedStores: Array<Store> = [];
  /** 
   * Array of the tags for the slider. Used to match the slider with the stores that have the same tags.
   */
  tags: Array<string> = [];

  /**
   * Empty constructor.
   * @param title Headline of the slider displayed in the center of front image.
   * @param description Main description of the slider displayed under title.
   * @param footer Short message of the slider displayed under description.
   * @param frontCover Introductory image displayed at the beginning of every slider.
   * @param storesCover Background image displayed behind all merchants in the slider.
   * @param backCover "See more" image that prompts the user to see more stores on this category.
   */
  constructor(
    private title: string,
    private description: string,
    private footer: string,
    private frontCover: string,
    private storesCover: string,
    private backCover: string
  ) {}

  /**
   * Sets the array of tags for this slider.
   */
  setTags(tags) {
    this.tags = tags;
  }

  /**
   * Converts store data fetched from Firestore to {@link Store} application models
   * and saves them in the corresponding store array of the slider.
   * @param stores Store data as fetched from Firestore.
   */
  setStoreData(stores: any) {
    stores.forEach(store => {
      let services: YummyService[] = [];
      if (store.services.cashback) services.push(new YummyService(1, store.cashbackPercentage));
      if (store.services.stamps) services.push(new YummyService(2));
      if (store.services.ibankpay) services.push(new YummyService(3));

      this.promotedStores.push(new Store(store.id, store.legalName, "https://www.yummywallet.com" + store.image, this.storesCover, services, {
        background: store.color,
        color: '#FFFFFF'
      }));
    });
  }
}
