import { Component } from '@angular/core';
import { NavController, NavParams } from "ionic-angular";
import { Store } from "../../models/store-model"
import { Events } from "ionic-angular";
/**
 * Shows store details and offers
 *
 * @summary This is the merchant details page
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 12:40:41 
 * Last modified  : 2018-03-09 15:33:50
 */
@Component({
  selector: 'store-details',
  templateUrl: 'store-details.html',
})
export class MerchantDetailsPage {
  /** Whether the user has a website or not */
  hasSite: boolean = true;

  /** Store data */
  store: Store;

  /**
   * Initializes merchant information
   * @param navCtrl Navigation Controller
   * @param navParams: {NavParams} gets the appropriate merchant
   * @param events: this parameter is used for changing the following status of the merchant
   */
  constructor(private events: Events, private navCtrl: NavController, private navParams: NavParams) {
    this.store = navParams.get('store');
    console.log(this.store);
  }

  /**
   * Adds or removes the store from user's "favourites" list
   */
  changeFollowingStatus(): void {
    this.store.changeFollowingStatus(this.events);
  }

  /**
   * Returns to home page
   */
  goBack(): void {
    this.navCtrl.pop();
  }
}
