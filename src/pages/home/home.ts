import { Component } from '@angular/core';
import { StoreSlider } from "../../models/store-slider-model";
import { GlobalVariables } from "../../providers/global-variables/global-variables-service";
import { SqlStorageService } from "../../providers/sql-storage/sql-storage-service";
/**
 * Home page of the application. Shows all the store sliders that are loaded.
 *
 * @summary Home page of the application. Shows all the store sliders that are loaded.
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 14:06:21 
 * Last modified  : 2018-03-09 15:23:23
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  /**
   * Array of all the store sliders loaded into the app.
   */
  storeSliders: Array<StoreSlider>;

  /**
   * Initializes store sliders in home page
   * @param global_variables Global variables application service
   * @param storage SQL storage service
   */
  constructor(
    private global_variables: GlobalVariables,
    private storage: SqlStorageService
  ) {
    this.storeSliders = this.storage.getSliders();
  }
}
