import { Component } from '@angular/core';
import { NavController, Events } from "ionic-angular";
import { SqlStorageService } from "../../providers/sql-storage/sql-storage-service";
import { TabsPage } from "../tabs/tabs";
/**
 * Acts as the main entry point of the application. Checks whether there is an active
 * internet connection and then:
 * 1) If there is an active connection, fetches all the necessary information from Firestore,
 * updating the progress bar with each completed request and when all of them are completed
 * pushes home page.
 * 2) If there is not an active connection, data are fetched from local SQL storage using
 * {@link SqlStorageService}.
 *
 * @summary Fetches data from Firestore or local storage
 * @author John Fanidis
 *
 * Created at     : 2018-03-09 13:44:06 
 * Last modified  : 2018-03-09 16:18:48
 */
@Component({
  selector: 'page-splash',
  templateUrl: 'splash-screen.html',
})
export class SplashPage {

  /** Current progress bar value */
  loadingProgress: number;

  /** Current progress bar message */
  loadingMessage: string;

  /**
   * Initializes private properties
   * @param navCtrl Navigation controller
   * @param storage SQL storage service
   * @param events Events emit service
   */
  constructor(
    private navCtrl: NavController,
    private storage: SqlStorageService,
    private events: Events
  ) {
    /** @todo if open progressive app through web browser show "unavailable page" */
    this.loadingProgress = 0;
    this.loadingMessage = 'Ετοιμάζουμε για εσάς τις καλύτερες προτάσεις'
  }

  /**
   * 1) Fetches data from Firestore if navigator is connected
   * to a network otherwise loads data saved in local storage.
   *
   * 2) Subscribes to observables to keep track of current progress
   * and when a subscription completes, increases progress bar value
   * and changes displayed message.
   */
  ionViewDidEnter() {
    if (navigator.onLine) {
      /** @todo add timeout interceptor */
      console.log("You connected to a network. Data will be fetched from Firestore.");
      this.storage.geStoreCategoriesFromFirestore();
      this.storage.getSlidersFromFirestore();
      this.storage.getFavouriteStores();
    } else {
      console.log("You are not connected to a network. Data will be fetched from local storage");
      this.storage.getStoreCategories();
      this.storage.getSliders();
      this.storage.getFavouriteStores();
    }

    this.events.subscribe('categories:loaded', categories => {
      this.updateProgress('Ψάχνουμε τα πιο ενδιαφέροντα σημεία της πόλης');
    });

    this.events.subscribe('favourites:loaded', favourites => {
      this.updateProgress('Εντοπίζουμε τα αγαπημένα σας καταστήματα');
    });

    this.events.subscribe('sliders:loaded', sliders => {
      this.updateProgress('Είστε έτοιμοι!');
    });
  }

  /**
   * When splash screen closes unsubscribes from all observables
   */
  ionViewDidLeave() {
    this.events.unsubscribe('categories:loaded');
    this.events.unsubscribe('sliders:loaded');
    this.events.unsubscribe('favourites:loaded');
  }

  /**
   * Updates the value of the progress bar and changes the displayed message.
   * @param newMessage The new message to be displayed
   */
  updateProgress(newMessage: string) {
    this.loadingMessage = newMessage;
    
    this.loadingProgress += 30;

    if (this.loadingProgress === 90) {
      this.navCtrl.setRoot(TabsPage);
    }
  }
}
