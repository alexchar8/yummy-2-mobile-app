import { Component } from '@angular/core';
import { GlobalVariables } from "../../providers/global-variables/global-variables-service";
import { AppVersion } from '@ionic-native/app-version';
/**
 * Shows extra features about application, video tutorials, guides and latest news.
 *
 * @summary Shows extra features about application, video tutorials, guides and latest news.
 * @author John Fanidis
 *
 * Created at     : 2018-02-13 17:17:52 
 * Last modified  : 2018-03-09 15:23:42
 */
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {
  /**
   * Array of list options to show in HTML template. 
   * Every object in options array has the following fields:
   * 1) icon: If the option is type of 'item' then it has a corresponding icon at the start of the row.
   * 2) type: An option can be of type 'header' or 'item' each with different presentation in HTML template.
   * 3) name: The title of the option.
   */
  options: Array<{icon?: string, type: string, name: string}>;
  
  /**
   * Sets the values for the options list and logs the application version.
   * @param globalVariables Global variables service
   * @param appVersion Current version of the application
   */
  constructor(
    private globalVariables: GlobalVariables,
    private appVersion: AppVersion) {

    this.options = globalVariables.getMorePageOptions();

    /** @todo uncomment when building for mobile */
    // this.appVersion.getVersionNumber().then(result=> {
    //   console.log(result);
    // });
  }
}
