import { Component } from '@angular/core';
import { AccountPage } from '../account/account';
import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';
import { Events } from 'ionic-angular';
import { FavouritePage } from '../favourite/favourite';
import { SqlStorageService } from '../../providers/sql-storage/sql-storage-service';
/**
 * Matches navigation tabs with corresponding application pages
 * and sets the number of unread notification messages for the currently logged in user (if any).
 * 
 * @summary Matches navigation tabs with corresponding application pages.
 * @author John Fanidis
 *
 * Created at     : 2018-02-13 15:39:58
 * Last modified  : 2018-03-09 19:42:36
 */
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  /** Home Tab leads to {@link HomePage} where the user can see the store sliders. */
  homeTab = HomePage;

  /** Favourite Tab that leads to {@link FavouritePage} where the user can see his favourite stores. */
  favouriteTab = FavouritePage;

  /** Search Tab that leads to {@link SearchPage} where the user can search stores. */
  searchTab = SearchPage;

  /**
   * Account tab leads to {@link AccountPage}
   * where the user can:
   * 1) Login if he is a guest 
   * 2) Check his account statistics if has logged in
   */
  accountTab = AccountPage;

  /**
   * Number of unread notification messages the currently logged in user (if any) has. 
   * It is displayed in the corresponding tab-badge in HTML template.
   */
  numberOfNotifications: number;

  /**
   * Number of new and unseen stores the user started following. 
   * It is displayed in the corresponding tab-badge in HTML template.
   */
  numberOfFollowingStores: number;
  
  /**
   * Sets the number of unread notification messages.
   * @todo Take unread notification messages number from Firestore.
   */
  constructor(
    private events: Events,
    private storage: SqlStorageService
  ) {

    this.numberOfNotifications = 0;
    let stores = this.storage.getFavouriteStores();
    this.numberOfFollowingStores = stores !== undefined ? stores.unseenNumber : 0;

    events.subscribe('store:started-following', store => {
      this.numberOfFollowingStores++;
    });

    events.subscribe('store:stopped-following', store => {
      if (this.numberOfFollowingStores > 0) {
        this.numberOfFollowingStores--;
      }
    });

    events.subscribe('favourites:reset', () => {
      this.numberOfFollowingStores = 0;
    });
  }
}
