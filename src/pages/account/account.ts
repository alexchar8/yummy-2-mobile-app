import { Component } from '@angular/core';
import { GlobalVariables } from '../../providers/global-variables/global-variables-service';
import { MorePage } from '../more/more';
import { NavController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { WithdrawPage } from '../withdraw/withdraw';
import { AngularFireAuth } from 'angularfire2/auth';
import { EmailComposer } from '@ionic-native/email-composer';

import * as firebase from 'firebase/app';

/**
 * Acts as two different pages depending on whether the user has logged in or not.
 * 1) If the user has not logged in, then this page acts as his entry point where he can register
 * for a new account or login with his credentials or connect with Facebook/Google account.
 * 2) If the user has logged in, then this pages acts as his central account hub where he
 * can see his profile card, statistics, notifications and most importantly manage his cash.
 *
 * @summary Central account management hub
 * @author John Fanidis
 *
 * Created at     : 2018-03-09 10:46:55 
 * Last modified  : 2018-03-14 09:50:31
 */
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  /** Array of actions available to user */
  accountActions: Array<{ id: number, icon: string, name: string }>;

  /** Used to determine session mode. Possible modes:
   * 1) Guest mode
   * 2) Yummy mode
   * 3) Facebook mode
   * 4) Google mode
   */
  sessionMode: string

  /**
   * Initializes actions array
   * @param navCtrl Navigation controller
   * @param global_variables Global variables service
   */
  constructor(
    private navCtrl: NavController,
    private global_variables: GlobalVariables,
    private afAuth: AngularFireAuth,
    private emailComposer: EmailComposer
  ) {
    this.sessionMode = global_variables.getSessionMode();
    this.accountActions = global_variables.getAccountOptions();
  }

  /**
   * Takes an action id as a parameter and pushes the corresponding page
   * to navigation controller.
   * @param action Action id (in html select box)
   */
  chooseAction(action) {
    switch (action.id) {
      case 2:
        this.navCtrl.push(WithdrawPage);
        break;
      case 4:
        this.navCtrl.push(SettingsPage);
        break;
      case 6:
        this.navCtrl.push(MorePage);
        break;
    }
  }

  loginWithFB() {
    let provider = new firebase.auth.FacebookAuthProvider();

    this.afAuth.auth.signInWithRedirect(provider).then(result => {
      console.log(result);
      // this.afAuth.auth.getRedirectResult().then(result => {
      //   console.log(result);
      // });
    });
  }

  loginWithGoogle() {
    let provider = new firebase.auth.GoogleAuthProvider();

    this.afAuth.auth.signInWithRedirect(provider).then(result => {
      console.log(result);
      // this.afAuth.auth.getRedirectResult().then(result => {
      //   console.log(result);
      // });
    });
  }

  sendSupportEmail() {
    let email = {
      to: 'yummy@yummywallet.com',
      cc: 'webmaster@yummywallet.com',
      bcc: ['g.papazidis@yummywallet.com', 'k.kampouraki@yummywallet.com'],
      subject: 'Yummy Wallet Support Team'
    };

    this.emailComposer.open(email);
  }
}
