import {Component} from '@angular/core';
import {NavController} from "ionic-angular";
import {GlobalVariables} from "../../providers/global-variables/global-variables-service";
/**
 * Shows the different options the user has available to manage his cash. Currrent options:
 * 1) Bank transfer
 * 2) Paypal transfer
 *
 * @summary Shows the different options the user has available to manage his cash
 * @author John Fanidis
 *
 * Created at     : 2018-03-09 13:41:55 
 * Last modified  : 2018-03-09 13:43:09
 */
@Component({
    selector: 'page-withdraw',
    templateUrl: 'withdraw.html',
})
export class WithdrawPage {

    /** Array of withdraw methods */
    withdrawMethods: any;

    /**
     * Initializes withdraw methods
     * @param navCtrl Navigation controller
     * @param global_variables Global variables service
     */
    constructor(public navCtrl: NavController, public global_variables: GlobalVariables) {
        this.withdrawMethods = global_variables.getWithdrawMethods();
    }
}
