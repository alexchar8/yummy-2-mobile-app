import { Component } from '@angular/core';
import { SqlStorageService } from '../../providers/sql-storage/sql-storage-service';
import { StoreCategory} from '../../interfaces/store-category'
/**
 * Provides various ways for the user to search application content
 * Current options:
 * 1) Category tiles grid
 * 2) Multiple filters (coming soon)
 * 3) dictionary search (coming soon)
 *
 * @summary Provides various ways for the user to search application content
 * @author John Fanidis
 *
 * Created at     : 2018-03-09 10:10:55 
 * Last modified  : 2018-03-09 16:45:29
 */
@Component({
    selector: 'page-search',
    templateUrl: 'search.html'
})
export class SearchPage {

    /** Array of application store categories */
    categories: StoreCategory[];

    /**
     * Fetches store categories from local sql storage
     * @param storage SQL storage service
     */
    constructor(
        private storage: SqlStorageService
    ) {
        this.categories = this.storage.getStoreCategories();
    }

    searchArray() {
    }
}
