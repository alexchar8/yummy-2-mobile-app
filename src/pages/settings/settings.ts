import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SqlStorageService} from "../../providers/sql-storage/sql-storage-service";
import {Events} from "ionic-angular";
import {GlobalVariables} from "../../providers/global-variables/global-variables-service";
/**
 * User and application settings page
 *
 * @summary User and application settings page
 * @author John Fanidis
 *
 * Created at     : 2018-03-08 18:18:47 
 * Last modified  : 2018-03-09 15:24:04
 */
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})

export class SettingsPage {
  /** The currently used language */
  language: string;

  /**
   * Initialize private variables
   * @param translate Translations service
   * @param events Events emit service
   * @param storage SQL storage service
   * @param global_variables Global variables service
   */
  constructor(private translate: TranslateService,
    private events: Events,
    private storage: SqlStorageService,
    private global_variables: GlobalVariables) {
    if (translate.currentLang === 'el') {
      this.language = 'el';
    } else {
      this.language = 'en';
    }
  }

  /**
   * Changes the application language
   * @param selectedValue the new language to be used
   */
  changeLanguage(selectedValue: any): void {
    this.translate.use(selectedValue);
  }
}
