import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { SqlStorageService } from '../../providers/sql-storage/sql-storage-service';
import { Store } from '../../models/store-model';
/**
 * Shows user's favourite stores for "one-click" access.
 *
 * @summary Shows user's favourite stores for "one-click" access
 * @author John Fanidis
 *
 * Created at     : 2018-03-09 10:58:38 
 * Last modified  : 2018-03-09 16:59:20
 */
@Component({
  selector: 'page-favourite',
  templateUrl: 'favourite.html'
})
export class FavouritePage {

  /**
   * Array of user's favourite stores
   */
  favouriteStores: Array<Store>

  /**
   * Empty constructor
   * @param events Application events service
   * @param storage SQL Storage service
   */
  constructor(
    private events: Events,
    private storage: SqlStorageService
  ) {}

  /**
   * Resets user's "new favourite" badge number
   */
  ionViewDidEnter() {
    let stores = this.storage.getFavouriteStores();
    this.favouriteStores = stores !== undefined && stores.data.length > 0 ? stores.data : null;
    console.log(this.favouriteStores);
    this.events.publish('favourites:reset');
  }
}
