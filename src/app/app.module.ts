import { NgModule, ErrorHandler, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SearchPage } from '../pages/search/search';
import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { MorePage } from "../pages/more/more";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashPage } from "../pages/splash-screen/splash-screen";
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { WithdrawPage } from "../pages/withdraw/withdraw";
import { SettingsPage } from "../pages/settings/settings";
import { HttpModule } from "@angular/http";
import { StoreCard } from '../components/store-card/store-card';
import { StoreSliderComponent } from '../components/store-slider/store-slider';
import { YummyServiceComponent } from '../components/yummy-service-button/yummy-service-button';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SentryErrorHandler } from "../handlers/sentry-error-handler";
import { SqlStorageService } from '../providers/sql-storage/sql-storage-service';
import { GlobalVariables } from '../providers/global-variables/global-variables-service';
import { IonicStorageModule } from '@ionic/storage';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { MerchantDetailsPage } from "../pages/store-details/store-details";
import { GoogleMaps } from "@ionic-native/google-maps";
import { FlashCardComponent } from "../components/flash-card/flash-card";
import { FirebaseBackendService } from '../providers/firebase-backend-service/firebase-backend-service';
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { IonicImageLoader } from 'ionic-image-loader';
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { AngularFireAuthModule } from "angularfire2/auth";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AppVersion } from '@ionic-native/app-version';
import { FavouritePage } from '../pages/favourite/favourite';
import { ServiceLocator } from '../handlers/service-locator';
import { EmailComposer } from '@ionic-native/email-composer';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

let config = {
  apiKey: "AIzaSyBOURs5UZFTU3j5w_SlZWzD-ZT0ddyHT8E",
  authDomain: "yummy-wallet-v2.firebaseapp.com",
  databaseURL: "https://yummy-wallet-v2.firebaseio.com",
  projectId: "yummy-wallet-v2",
  storageBucket: "yummy-wallet-v2.appspot.com",
  messagingSenderId: "237678150707"
};

@NgModule({
  declarations: [
    MyApp,
    SearchPage,
    AccountPage,
    HomePage,
    TabsPage,
    MorePage,
    SplashPage,
    WithdrawPage,
    MorePage,
    SettingsPage,
    StoreCard,
    StoreSliderComponent,
    YummyServiceComponent,
    FlashCardComponent,
    MerchantDetailsPage,
    ProgressBarComponent,
    FavouritePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    LazyLoadImageModule,
    AngularFireAuthModule,
    IonicImageViewerModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: "true" }),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SearchPage,
    AccountPage,
    HomePage,
    TabsPage,
    MorePage,
    SettingsPage,
    WithdrawPage,
    SplashPage,
    MerchantDetailsPage,
    FavouritePage
  ],
  providers: [
    /**
     * SentryErrorHandler is not needed while in development mode (error are checked in debug/console)
     * Switch to SentryErrorHandler when building release versions (errors are sent to Sentry)
     */
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    // {provide: ErrorHandler, useClass: SentryErrorHandler},
    StatusBar,
    GoogleMaps,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SqlStorageService,
    GlobalVariables,
    HttpModule,
    FirebaseBackendService,
    BarcodeScanner,
    AppVersion,
    EmailComposer
  ]
})

export class AppModule {
  constructor(private injector: Injector) {    // Create global Service Injector.
    ServiceLocator.injector = this.injector;
  }
}

