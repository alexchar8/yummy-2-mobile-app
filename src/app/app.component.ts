import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashPage } from "../pages/splash-screen/splash-screen";
import { Platform } from "ionic-angular";
import { TranslateService } from '@ngx-translate/core';
/**
 * Starting component of the application. It handles plugin initialization, translations and setup of root page
 *
 * @summary short description for the file
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 15:40:30 
 * Last modified  : 2018-03-09 15:19:37
 */
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  /**
   * Yummy custom splash screen is the first page to load when app opens
   */
  rootPage = SplashPage;
  /**
   * Entry point of the application. Loads all available cordova plugins.
   * @param platform The platform we are initializing.
   * @param statusBar Status bar component (applicable only to mobile platforms).
   * @param translate Translate service
   */
  constructor(private platform: Platform, private statusBar: StatusBar, private translate: TranslateService) {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });

    this.setupLanguages(this.translate);
  }

  /**
   * Set the languages we are going to use in the app and which is the default. Keys passed in addLangs array
   * match the ones we give to the locale json files which contain all the translations (see in src/assets/i18n).
   * @param {TranslateService} translate Global translate service of the application
   */
  setupLanguages(translate: TranslateService) {
    translate.addLangs(['el', 'en']);
    translate.use('en');
  }
}
