/**
 * Represents a yummy store category fetched from Firestore
 *
 * @summary Represents a yummy store category fetched from Firestore
 * @author John Fanidis
 *
 * Created at     : 2018-03-02 18:45:35 
 * Last modified  : 2018-03-03 01:59:58
 */
export interface StoreCategory {
    /** Category title displayed over image */
    title: string,
    /** Category image displayed as background*/
    logo: string
}