import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
/**
 * Stores private application data that all components can access through public methods
 *
 * @summary Stores private application data that all components can access through public methods
 * @author John Fanidis
 *
 * Created at     : 2018-02-14 10:09:30 
 * Last modified  : 2018-03-09 15:26:30
 */
@Injectable()
export class GlobalVariables {

  /** Array of segments in {@link HomePage} */
  listOfSegments: Array<{ name: string }>;
  
  /** * Array of choices in {@link AccountPage} */
  myAccountOptions: Array<{ id: number, icon: string, name: string }>;

  /** Array of choices in {@link MorePage} */
  morePageOptions: Array<{ icon?: string, type: string, name: string }>;

  /** Array of withdraw methods in {@link WithdrawPage} */
  withdrawMethods: Array<{name: string, image: string}>;

  /** Current application domain used for HTTP calls. */
  domain: string = 'https://www.yummywallet.com';

  /** Used to determine session mode. Possible modes:
   * 1) Guest mode
   * 2) Yummy mode
   * 3) Facebook mode
   * 4) Google mode
   */
  sessionMode: string

  /** Initializes all arrays with values. */
  constructor() {
    this.sessionMode = 'Guest'
    this.listOfSegments = [
      {
        name: 'nearby'
      },
      {
        name: 'following'
      }];
    this.myAccountOptions = [
      {
        id: 1,
        icon: 'stats',
        name: 'VIEW_ACTIVITY'
      },
      {
        id: 2,
        icon: 'card',
        name: 'MANAGE_YOUR_CASH'
      },
      {
        id: 3,
        icon: 'school',
        name: 'INSTITUTION'
      },
      {
        id: 4,
        icon: 'settings',
        name: 'SETTINGS'
      },
      {
        id: 5,
        icon: 'notifications',
        name: 'NOTIFICATIONS'
      },
      {
        id: 6,
        icon: 'help-buoy',
        name: 'ABOUT'
      }
    ];
    this.morePageOptions = [
      {
        type: 'header',
        name: 'ABOUT'
      },
      {
        icon: 'book',
        type: 'item',
        name: 'GUIDE'
      },
      {
        icon: 'appstore',
        type: 'item',
        name: 'NEW'
      },
      {
        icon: 'megaphone',
        type: 'item',
        name: 'FEEDBACK'
      },
      {
        type: 'header',
        name: 'PRIVACY'
      },
      {
        icon: 'paper',
        type: 'item',
        name: 'TERMS'
      }
    ];
    this.withdrawMethods = [
      {
        name: 'BANK',
        image: 'assets/images/bank.png'
      },
      {
        name: 'PAYPAL',
        image: 'assets/images/paypal.png'
      }
    ];
  }

  /** 
   * Returns the currently selected application domain
   * @return {string}
   */
  getAppDomain(): string {
    return this.domain;
  }

  /**
   * Returns an array of choices visible in More page 
   * @return {Array<{ icon?: string, type: string, name: string }>}
   */
  getMorePageOptions(): Array<{ icon?: string, type: string, name: string }> {
    return this.morePageOptions;
  }

  /**
   * Returns an array of segments visible in Home page
   * @return {Array<{ name: string }>}
   */
  getSegments(): Array<{ name: string }> {
    return this.listOfSegments;
  }

  /** 
   * Returns an array of choices visible in My Account page
   * @return {Array<{ id: number, icon: string, name: string }>}
   */
  getAccountOptions(): Array<{ id: number, icon: string, name: string }> {
    return this.myAccountOptions;
  }

  /** 
   * Returns an array of withdraw methods visible in Withdraw page
   * @return {Array<{name: string, image: string}>}
   */
  getWithdrawMethods(): Array<{name: string, image: string}> {
    return this.withdrawMethods;
  }

  getSessionMode() {
    return this.sessionMode;
  }
}
