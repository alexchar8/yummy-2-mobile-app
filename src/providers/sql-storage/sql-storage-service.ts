import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from "ionic-angular";
import { FirebaseBackendService } from "../firebase-backend-service/firebase-backend-service";
import { StoreSlider } from '../../models/store-slider-model';
import { StoreCategory } from '../../interfaces/store-category'
import { Store } from '../../models/store-model';
import { User } from '../../models/user-model';
/**
 * Communicates with the application for saving/getting data. Fetches data from
 * Firestore, saves them in local SQL database and caches them in runtime variables.
 * After initial app load, each time a module requests data, service returns the
 * runtime values instead of making another HTTP call or SQL storage fetch.
 *
 * @summary Saves HTTP results to local SQL storage and runtime variables that are
 * shared with the rest application modules.
 * @author John Fanidis
 *
 * Created at     : 2017-09-10 13:03:44 
 * Last modified  : 2018-03-09 19:13:08
 */

@Injectable()
export class SqlStorageService {
  /** Used for caching store slider results */
  categories: StoreCategory[];

  /** Used for caching store slider results */
  sliders: StoreSlider[];

  /** Used for caching favourite stores and how many of them are unseen*/
  favourites: { data: Store[], unseenNumber: number };

  /** Used for caching user data */
  user: User

  /**
   * Initializes necessary services for communicating with Firestore and saving to storage
   * @param storage SQL storage driver
   * @param events Event subscription service
   * @param firestoreBackend Firestore backend service
   */
  constructor(
    private storage: Storage,
    private events: Events,
    private firestoreBackend: FirebaseBackendService) {
    events.subscribe('store:started-following', store => {
      this.saveNewFavouriteStoreToStorage(store);
    });
    events.subscribe('store:stopped-following', store => {
      this.removeFavouriteStoreToStorage(store);
    });
    events.subscribe('favourites:reset', () => {
      this.resetNewFavouriteStores();
    });
  }

  /** 
   * Returns the enabled store categories from local SQL database (if available)
   * and caches them in a runtime variable. If data are not available in SQL database
   * a Firestore call is made in order to retrieve them.
   * @returns {StoreCategory[]}
   */
  getStoreCategories(): StoreCategory[] {
    if (this.categories) {
      console.log('Categories were already loaded. No local storage or server needed');
      return this.categories;
    } else {
      this.storage.get('categories').then(categories => {
        if (categories) {
          this.categories = categories;
          this.events.publish('categories:loaded', categories);
          console.log('Categories loaded from SQL storage');
        } else {
          this.geStoreCategoriesFromFirestore();
        }
      }).catch(error => {
        console.log(error);
        this.geStoreCategoriesFromFirestore();
      });
    }
  }

  /** 
   * Fetches store categories from Firestore and publishes an event when loading is completed.
   */
  geStoreCategoriesFromFirestore() {
    this.firestoreBackend.getStoreCategories().then(categories => {
      console.log('Categories loaded from Firestore');
      this.categories = categories;
      this.events.publish('categories:loaded', categories);
      this.storage.set('categories', categories);
    }).catch(error => {
      console.log(error);
      this.getStoreCategories();
    });
  }

  /** 
   * Fetches the visible sliders from local SQL database (if available)
   * and caches them in a runtime variable. If data are not available in SQL database
   * a Firestore call is made in order to retrieve them.
   * @returns {StoreSlider[]} 
   */
  getSliders(): StoreSlider[] {
    if (this.sliders) {
      console.log('Sliders were already loaded. No local storage or server needed');
      return this.sliders;
    } else {
      this.storage.get('sliders').then(sliders => {
        if (sliders) {
          this.sliders = sliders;
          this.events.publish('sliders:loaded', sliders);
          console.log('Sliders loaded from SQL storage');
        }
      });
    }
  }

  /**
   * Fetches sliders from Firestore and publishes an event when loading is completed
   */
  getSlidersFromFirestore() {
    this.firestoreBackend.getSliders().then(sliders => {
      console.log('Sliders loaded from Firestore');
      this.sliders = sliders;
      this.events.publish('sliders:loaded', sliders);
      this.storage.set('sliders', sliders);
    }).catch(error => {
      console.log(error);
      this.getSliders();
    });
  }

  /**
   * Saves a new favourite store in device storage
   * @param store Store to be added in storage favourites
   */
  saveNewFavouriteStoreToStorage(store: Store) {
    let savedStores = [];
    let unseenNumber = 0;
    this.storage.get('favourites').then(result => {
      if (result !== null) {
        savedStores = result.data;
        unseenNumber = result.unseenNumber
      }
      unseenNumber = result === null ? 1 : result.unseenNumber + 1;
      savedStores.push(store);
      this.favourites = {
        data: savedStores,
        unseenNumber: unseenNumber
      };
      this.storage.set('favourites', this.favourites);
    });
  }

  /**
   * Removes a favourite store from device storage
   * @param store Store to be removed from storage favourites
   */
  removeFavouriteStoreToStorage(store: Store) {
    this.storage.get('favourites').then(result => {
      let savedStores = result;
      result.data.forEach((data, index) => {
        if (data.id === store.getId()) {
          savedStores.data.splice(index, 1);
          this.favourites = savedStores;
          this.storage.set('favourites', savedStores);
        }
      });
    });
  }

  /**
   * Fetches the favourite stores from local SQL database
   * and saves them in a runtime variable.
   */
  getFavouriteStores() {
    if (this.favourites) {
      console.log('Favourite stores were already loaded. No local storage or server needed');
      console.log(this.favourites);
      return this.favourites;
    } else {
      this.storage.get('favourites').then(favourites => {
        this.favourites = favourites; //save results in a runtime variable
        this.events.publish('favourites:loaded', favourites);
        console.log('Favourite stores loaded from SQL storage');
      });
    }
  }

  /**
   * Sets unseen favourites stores number to zero and saves to storage.
   */
  resetNewFavouriteStores() {
    if (this.favourites !== null) {
      this.favourites.unseenNumber = 0;
      this.storage.set('favourites', this.favourites);
    }
  }

  /**
   * Takes a store id as a parameter and checks whether this store is
   * in user's "favourite" list or not.
   * @param id Store unique id
   */
  isStoreInFavourites(id: number): boolean {
    let found = false;
    if (this.favourites) {
      this.favourites.data.forEach(store => {
        console.log(store['id'])
        if (store['id'] === id) {
          found = true;
          return;
        };
      });
    }
    return found;
  }
}
