import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFirestore } from "angularfire2/firestore";
import { StoreSlider } from '../../models/store-slider-model';
import { Store } from '../../models/store-model';
import { StoreCategory } from '../../interfaces/store-category'
import { SqlStorageService } from '../sql-storage/sql-storage-service';
/**
 * Uses methods to retrieve data from Google Firestore and maps them to
 * local models in order for them to be accessible throughtout the application
 *
 * @summary Retrieves data from Firestore
 * @author John Fanidis
 *
 * Created at     : 2018-03-03 02:00:06 
 * Last modified  : 2018-03-08 15:43:23
 */

@Injectable()
export class FirebaseBackendService {

  /**
   * Empty constructor
   * @param http Http client service
   * @param db Firestore database reference
   */
  constructor(private http: Http, private db: AngularFirestore, private injector: Injector) { }

  /**
   * Retrieves sliders from Firestore, adds stores to each one of them
   * based on it's array of 'tags' and finally converts the Firestore data
   * to application {@link Store} models.
   */
  getSliders(): Promise<any> {
    return new Promise((resolve, reject) => {
      let ref = this.db.collection<any>("sliders", ref => ref.where('visible', '==', true).orderBy('position')).valueChanges().subscribe(firestoreSliders => {
        let promises = [];
        /** For each slider retrieved...*/
        firestoreSliders.forEach(firestoreSlider => {
          /**...loop through it's array of tags...*/
          firestoreSlider.tags.forEach(tag => {
            /**...add the promise that returns stores based on tag 
             * in a array of promises (don't execute the prοmises)...*/
            promises.push(this.assignStoresByTag(firestoreSlider, 'tags.' + tag));
          });
        });
        /**...finally execute all promises and convert firestore data to application {@link Store} models*/
        Promise.all(promises).then(result => {
          let yummySliders: Array<StoreSlider> = [];
          firestoreSliders.forEach(firestoreSlider => {
            let yummySlider = new StoreSlider(
              firestoreSlider.title,
              firestoreSlider.description,
              firestoreSlider.footer,
              firestoreSlider.frontCover,
              firestoreSlider.storesCover,
              firestoreSlider.backCover
            );
            yummySlider.setTags(firestoreSlider.tags);
            yummySlider.setStoreData(firestoreSlider.storeData);

            yummySliders.push(yummySlider);
          });
          resolve(yummySliders);
          ref.unsubscribe();
        });
      });
    });
  }

  /**
   * Fills a slider object with store data based on a specific tag.
   * Example:
   * If a store has two tags 'new' and 'hot' then this function is called
   * twice taking all the stores that have each of the tags and adding them
   * to slider data.
   * @param {any} slider Slider that we want to fill with stores
   * @param {String} tag The tag based on which we want to search stores for the given slider
   */
  assignStoresByTag(slider: any, tag: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.collection<Store>("stores", ref => ref.where(tag, '==', true)).valueChanges().subscribe(stores => {
        slider.storeData = stores;
        resolve(stores);
      });
    });
  }

  /**  
   * Shuffles array in place. ES6 version
   * @param {Array} a items An array containing the items.
   */
  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  /**
   * Retrieves store categories from Firestore
   */
  getStoreCategories(): Promise<any> {
    return new Promise((resolve, reject) => {
      let ref = this.db.collection<StoreCategory>("categories", ref => ref.orderBy('position')).valueChanges().subscribe(categories => {
        ref.unsubscribe();
        resolve(categories);
      });
    });
  }
}
