import { IonicErrorHandler } from 'ionic-angular';
import Raven from 'raven-js';

Raven.config(this.SENTRY_DSN,
  {
    release: this.SENTRY_RELEASE,
    /**
     * We upload the code source maps to Sentry (uploaded externally). This function cuts the prefixes of all uploaded
     * files' names from "../yummy-wallet-2/src/www/main.js" to "/main.js" (same for all source maps)
     */
    dataCallback: data => {
      if (data.culprit) {
        data.culprit = data.culprit.substring(data.culprit.lastIndexOf('/'));
      }
      let stacktrace = data.stacktrace ||
        data.exception &&
        data.exception.values[0].stacktrace;
      if (stacktrace) {
        stacktrace.frames.forEach(function (frame) {
          frame.filename = frame.filename.substring(frame.filename.lastIndexOf('/'));
        });
      }
    }
  }).install();

/**
* Sends all errors that happen in the application to the open source service Sentry with the use of raven.js library
* Default error handler is changed in the app.module.ts
*
* @summary Changes default error reporting tool to Sentry.js
* @author John Fanidis
*
* Created at     : 2018-03-08 18:09:17 
 * Last modified  : 2018-03-09 15:21:46
*/
export class SentryErrorHandler extends IonicErrorHandler {
  /**
   * Sentry application api key used for tool configuration
   */
  SENTRY_DSN: string = 'https://cc3bf63f7a8c47318b0f83ca61e72a86@sentry.io/213365';

  /**
   * Sentry version used for error reporting tool configuration
   */
  SENTRY_RELEASE: string = '1.0.0';
  
  /**
   * We extend the default ionic error handler and call his super method before sending the exception to Sentry
   * so we can print the error logs in our console during development
   */
  handleError(error) {
    super.handleError(error);

    try {
      Raven.captureException(error.originalError || error);
    }
    catch (e) {
      console.error(e);
    }
  }
}

